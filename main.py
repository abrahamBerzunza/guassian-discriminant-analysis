from LectorDescriptores import LectorDescriptores
import math
import numpy as np
from numpy.linalg import inv
import matplotlib.pyplot as plt
import pandas as pd

def calculate_mu(x, y, flag_mu):
  ans1 = np.dot(y.transpose(), x).transpose()
  ans2 = sum(y == flag_mu)[0]
  return ans1 / ans2


def calculate_sigma(x, y, mu0, mu1):
  sigma = np.zeros((7, 7))

  for i in range(1):
    _x = np.zeros((7,1))
    _x[0,0] = x[i, 0]
    _x[1,0] = x[i, 1]
    _x[2,0] = x[i, 2]
    _x[3,0] = x[i, 3]
    _x[4,0] = x[i, 4]
    _x[5,0] = x[i, 5]
    _x[6,0] = x[i, 6]
    
    if(y[i, 0] == 1):
      diff = np.subtract(_x, mu1)
    else:
      diff = np.subtract(_x, mu0)
    mult = np.dot(diff, diff.transpose())
    sigma = np.add(sigma, mult)

  sigma = np.divide(sigma, len(y))
  return sigma


def plot_data(vector, label):
  df = pd.DataFrame(dict(
    v1=vector[:,0].transpose(), 
    v2=vector[:,1].transpose(), 
    L=label.transpose().flatten()
  ))
  plt.scatter(df.v1[df.L == 1], df.v2[df.L == 1], c='red', marker='o', label = 'Semilla tipo 1')
  plt.scatter(df.v1[df.L == 0], df.v2[df.L == 0], c='aqua', marker='o', label = 'Semilla tipo 2')
  plt.title('GDA')
  legend = plt.legend(loc='lower right', fontsize='small')
  plt.show()


def decision_boundary(x, y, phi, mu0, mu1, sigma):
  Z = np.zeros(x.shape)
  for i in range(x.shape[0] - 1):
    for j in range(x.shape[1] -1):
      X = np.matrix([ 
        x[i,j], y[i,j], x[i,j], y[i,j], x[i,j], y[i,j], x[i,j] 
      ])
      X = X.transpose()
      diff1 = np.subtract(X, mu1)
      diff0 = np.subtract(X, mu0)
      first = np.dot(inv(sigma), diff1)
      first = np.dot(diff1.transpose(), first)[0,0]
      second = np.dot(inv(sigma), diff0)
      second = np.dot(diff0.transpose(), second)[0,0]
      constant = math.log(phi*1.0/(1.0 - phi))
      det1 = np.linalg.det(sigma)
      det0 = np.linalg.det(sigma)
      constant = constant - math.log(math.sqrt((det1 * 1.0)/det0))
      constant = 2 * constant
      Z[i,j] = (first - second - constant)
  return Z


def plot_decision_boundary(phi, mu0, mu1, sigma):
  a_range = np.arange(-10, 10, 0.25)
  b_range = np.arange(-10, 10, 0.25)
  a, b = np.meshgrid( a_range, b_range )
  z = decision_boundary(a, b, phi, mu0, mu1, sigma)
  cs = plt.contour(a, b, z, [0], colors='green',linestyles='dashed')
  labels = ['Límite de decisión']
  cs.collections[0].set_label(labels[0])


def main():
  reader = LectorDescriptores()
  data = reader.cargarDatos()
  X = []
  Y = []

  # Convert to numpy array
  for i in range(len(data)):
    X.append( data[i].vectorDescriptores )
    Y.append( data[i].nombreImagen )
  X = np.asarray(X)
  Y = np.asarray(Y)

  # Standarize vector
  for i in range(7):
    mean = np.mean(X[:,i])
    var = np.var(X[:,i])
    X[:,i] = (X[:,i] - mean / math.sqrt(var))

  # Calculating parameters
  phi = 0.5 #balanced data
  mu1 = calculate_mu(X, Y, 1)
  mu0 = calculate_mu(X, (1 - Y), 0)
  sigma = calculate_sigma(X, Y, mu0, mu1)

  print('')
  print('Media 1')
  print(mu1)

  print('\nMedia 0')
  print(mu0)

  print('\nsigma')
  print(sigma)
  
  plot_decision_boundary(phi, mu0, mu1, sigma)
  plot_data(X, Y)


if __name__ == "__main__":
  main()
